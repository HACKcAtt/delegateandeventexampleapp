﻿using System;

namespace DelegateAndEventExampleApp
{
    /// <summary>
    /// Делегаты представляют такие объекты, которые указывают на методы. То есть делегаты - это указатели на методы и с помощью делегатов мы можем вызвать данные методы.
    /// Анонимные методы используются для создания экземпляров делегатов.
    /// Определение анонимных методов начинается с ключевого слова delegate, после которого идет в скобках список параметров и тело метода в фигурных скобках.
    /// События, сигнализируют системе о том, что произошло определенное действие.
    /// В данном примере раскрывается суть использования делегатов, анонимных методов и событий.
    /// </summary>
    class Program
    {
        // Messages delegate declaration.
        public delegate void GetMessage();
        // Event declaration - a signal about showing "Good Morning" message. Making it static due to not creating class instance.
        public static event GetMessage GoodMorningIsShown;
        // Delegate of the anonymous method.
        delegate int OperationAnonymous(int x, int y);
        // Math operations delegate declaration.
        delegate int Operation(int x, int y);
        // Event using.
        public static void InvokeGoodMorning()
        {
            GoodMorningIsShown?.Invoke();
        }
        static void Main(string[] args)
        {
            // Anonymous method
            OperationAnonymous operationAnonymous = delegate (int x, int y)
            {
                return x - y;
            };
            int resultOfTheAnonymousMethod = operationAnonymous(10, 5);
            // Result will be 5 (because 10 - 5 == 5).
            Console.WriteLine(resultOfTheAnonymousMethod);

            // Assigning method address by constructor (first time let it be delegate of Add method).
            Operation delegateOperation = new Operation(Add);
            int result = delegateOperation.Invoke(4, 5);
            // Result will be 9 (because 4 + 5 == 9).
            Console.WriteLine(result);

            // Now reassigning deleagte to Multiply method.
            delegateOperation = Multiply;
            result = delegateOperation.Invoke(4, 5);
            // Result will be 20 (because 4 * 5 == 20).
            Console.WriteLine(result);
            
            // Delegate variable.
            GetMessage delegateVariable = null;
            // If current hur is behind 04:00 and 11:00.
            if (DateTime.Now.Hour >= 4 && DateTime.Now.Hour <= 11)
            {
                // Calling a methdo which is using event.
                GoodMorningIsShown += GoodMorning;
                InvokeGoodMorning();
                // Using delegate as a method parameter.
                ShowMessage(GoodMorning);
            }
            // If current hur is behind 17:00 and 22:00.
            if (DateTime.Now.Hour >= 17 && DateTime.Now.Hour <= 22)
            {
                // Assigninig the method address to the delegate variable.
                delegateVariable = GoodEvening;
            }
            // If current hur is behind 11:00 and 17:00.
            if (DateTime.Now.Hour > 11 && DateTime.Now.Hour < 17)
            {
                // Assigninig the method address to the delegate variable.
                delegateVariable = GoodDay;
            }
            // If current hur is behind 22:00 and 04:00.
            if (DateTime.Now.Hour > 22 && DateTime.Now.Hour < 4)
            {
                // Assigninig the method address to the delegate variable.
                delegateVariable = GoodNight;
            }
            delegateVariable.Invoke();
            Console.Read();
        }
        private static void GoodMorning()
        {
            // This message will be shown if it's morning now.
            Console.WriteLine("Good Morning.");
        }
        private static void GoodEvening()
        {
            // This message will be shown if it's evening now.
            Console.WriteLine("Good Evening");
        }
        private static void GoodDay()
        {
            // This message will be shown if it's day now.
            Console.WriteLine("Good Day");
        }
        private static void GoodNight()
        {
            // This message will be shown if it's day now.
            Console.WriteLine("Good Night");
        }
        // Delegate as a method parameter.
        private static void ShowMessage(GetMessage _del)
        {
            _del.Invoke();
        }
        private static int Add(int x, int y)
        {
            return x + y;
        }
        private static int Multiply(int x, int y)
        {
            return x * y;
        }
    }
}
